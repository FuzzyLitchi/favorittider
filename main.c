#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool is_arithmetic(int *array) {
    int size = *(array);
    int * display = array + 1;

    //Determine potiential difference
    int diff = display[1] - display[0];

    for (size_t i = 2; i < size; i++) {
        // i refers to which element we are checking.
        // We start with 2 because diff literally came from 0 and 1
        // so we only have to check 1&2 and maybe 2&3
        int potiential_diff = display[i] - display[i-1];
        if (diff != potiential_diff) {
            return false;
        }
    }

    return true;
}

bool is_geometric_rising(int *array) {
    int size = *(array);
    int * display = array + 1;

    //Rising factor
    if (display[0] == 0) {
        return false;
    }
    int factor = display[1] / display[0];

    if (factor == 0) {
        return false;
    }

    for (size_t i = 1; i < size; i++) {
        if (display[i] != factor*display[i-1]) {
            return false;
        }
    }

    return true;
}

bool is_geometric_falling(int *array) {
    int size = *(array);
    int * display = array + 1;

    //Rising factor
    if (display[1] == 0) {
        return false;
    }
    int factor = display[0] / display[1];

    if (factor == 0) {
        return false;
    }

    for (size_t i = 1; i < size; i++) {
        if (display[i-1] != factor*display[i]) {
            return false;
        }
    }

    return true;
}

int * displayed_time(int minutes_epoch) {
    int * array;

    int minutes = minutes_epoch % 60;
    int hours = (minutes_epoch / 60) % 12;

    if (hours == 0) {
        hours = 12;
    }

    if (hours >= 10) {
        array = malloc(5*sizeof(int));
        array[0] = 4;
        array[1] = hours / 10;
        array[2] = hours % 10;
        array[3] = minutes / 10;
        array[4] = minutes % 10;
    } else {
        array = malloc(4*sizeof(int));
        array[0] = 3;
        array[1] = hours;
        array[2] = minutes / 10;
        array[3] = minutes % 10;
    }

    return array;
}

void print_time(int minutes_epoch) {
    int minutes = minutes_epoch % 60;
    int hours = (minutes_epoch / 60) % 12;

    if (hours == 0) {
        hours = 12;
    }

    printf("%d:%02d\n", hours, minutes);
}

#define HOURS_IN_MINUTES 720
#define TIMES_PER_DAY 38

int main() {
    int minutes_epoch;
    scanf("%d", &minutes_epoch);

    int count = 0;

    //Optimized counting
    count += minutes_epoch / HOURS_IN_MINUTES * TIMES_PER_DAY;

    for (size_t i = 0; i <= minutes_epoch % 720; i++) {
        int * array = displayed_time(i);

        /*printf("%d %d %d %d", array[0], array[1], array[2], array[3]);
        if (array[0] == 4) {
            printf(" %d\n", array[4]);
        } else {
            printf("\n");
        }*/

        if (is_arithmetic(array)) {
            //printf("It's arthematic: ");
            //print_time(i);
            count++;
        } else if(is_geometric_rising(array)) {
            //printf("It's geometric rising: ");
            //print_time(i);
            count++;
        } else if(is_geometric_falling(array)) {
            //printf("It's geometric falling: ");
            //print_time(i);
            count++;
        } else {
            //printf("NOTHING!\n");
        }

        free(array);
    }

    printf("%d\n", count);

    return 0;
}
